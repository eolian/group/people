# Eolian People

A repository of people involved in Eolian and its group entities, including past involvements. 

Each person is provided with a folder that can contain a markdown file for a profile description (at a minimum) and an `/images` folder for profile pictures. 